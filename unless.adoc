=== unless

Unless is another way to check a condition.  Let say that one is a minor unless he or she is greater than 18 years old. So how to code it in Ruby? Consider the program below, type it in a text editor and execute it.

[source, ruby]
----
include::code/unless.rb[]
----

When executed this is what we get

----
Enter your age:16 
"You are a minor"
----
 
The program asks your age, it says you are minor if age is not greater than 18. That is it says you are a minor if unless your age is greater than or equal to 18 as shown in this condition `unless age >= 18`. The `p` is a kind of short form for putsfootnote:[This is 	not the right definition, but just remember it in that way]. If you write `puts “something”` , the ruby interpreter prints `something`. If you use `p ”something”` , the ruby interpreter prints `”something”` (tht is with those quotes).

If we want to put more than a line of code under an `unless` block we can use `unless .... end` block as shown below

----
unless <condition>
  # many lines of code goes here
end
----

The code in the block gets executed if the `<condition>` fails. `unless` can be thought as opposite of `if`. A if block gets executed `if` the condition in it is `true`, a `unless` block gets executed if the condition in it is `false`.

