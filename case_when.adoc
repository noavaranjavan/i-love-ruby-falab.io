=== case when

Suppose you want to write a program that has a determined output for determined input, you can use the case when. Lets  say that we want to write a program that spells from 1 to 5, we can do it as shown in code/case_when.rb[case_when.rb] , type the program in text editor and execute it.

[source, ruby]
----
include::code/case_when.rb[]
----

Output

----
Enter a number (1-5):4 
The number you entered is four 
----

Lets see how the above program works. First the user is prompted to enter a number, when he does enters a number, it gets converted from String to Integer in the following statement

[source, ruby]
----
a = gets.to_i
----

The variable `a` now contains the value of number we have entered, we have the `case` statement as shown

[source, ruby]
----
case a
  …......
end
----

In the above empty case statement we are going to write code that gets executed depending on the value of `a`. When `a` is 1 we need to spell out as `“one”` so we add the following code

[source, ruby]
----
case a
  when 1
  spell = "one"
end
----

Similarly we add code till the case is 5 as shown

[source, ruby]
----
case a
  when 1
  spell = "one"
  when 2
  spell = "two"
  when 3
  spell = "three"
  when 4
  spell = "four"
  when 5
  spell = "five"
end
----

There could be a case when the human who runs this program could give a wrong input, so we need to deal with those cases too. For that we add a special statement called `else`, if all the when cases fails, the code under else is executed, it must however be noted that its not mandatory to have an `else` between `case … end` block. So now the program changes as shown

[source, ruby]
----
case a
  when 1
  spell = "one"
  when 2
  spell = "two"
  when 3
  spell = "three"
  when 4
  spell = "four"
  when 5
  spell = "five"
  else
  spell = nil
end
----

Next all we must do is to print out `spell` which we do it in the following statements

[source, ruby]
----
puts "The number you entered is "+spell if spell
----

Note that we print out only if `spell` contains a value, else if `spell` is `nil` nothing is printed. It is taken care by the `if` condition in statement above.

Sometimes it might be necessary that we need to execute same set of statements for many conditions. Lets take a sample application in which the program determines a number from 1 to 10 (both inclusive) is odd or even. Type the code below (code/case_odd_even.rb[case_odd_even.rb]) and execute it

[source, ruby]
----
include::code/case_odd_even.rb[]
----

Output

----
7 is odd
----
 
Notice that in above program we assign a value 7 to a variable `num`, next we put the `num` in a `case` statement. When the number is 1, 3, 5, 7 and 9 we need to print its odd so all we do is to group the cases. When its satisfied it must print as `odd`, for that its just enough if you put it as shown in code below

[source, ruby]
----
case num
  when 1, 3, 5, 7, 9
  puts "#{num} is odd"
end
----

Next all we need to print the number is even if its 2, 4, 6, 8 and 10, to do this task all we need to do is to add code that highlighted below

[source, ruby]
----
case num
  when 1, 3, 5, 7, 9
  puts "#{num} is odd"
  when 2, 4, 6, 8, 10
  puts "#{num} is even"
end
----

Thats it. The code will work fine for all numbers from 1 to 10. The moral of the story is we can easily group  cases and execute a common code under it.

