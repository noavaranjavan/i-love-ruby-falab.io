Gem::Specification.new do |s|
  s.name        = 'shapes'
  s.version     = '0.0.0'
  s.date        = '2018-12-02'
  s.summary     = "A gem to calculate area and perimeter of shapes."
  s.authors     = ["Karthikeyan A K"]
  s.email       = 'mindaslab@protonmail.com'
  s.files       = Dir["*/*.rb", "*/*/*.rb"]
end
