require "shapes"

square = Square.new
square.side = 7
puts "Area of square = #{square.area}"

circle = Circle.new
circle.radius = 7
puts "Area of circle = #{circle.area}"
