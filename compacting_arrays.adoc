=== Compacting Arrays

Sometimes an array would contain `nil` values and you want to remove it, in that case you  can call `compact` upon that array. Lets try out an example. Start your irb or pry and type the following

First lets initialize the array

[source, ruby]
----
>> a = [1, nil, 2, nil, 3, nil]
=> [1, nil, 2, nil, 3, nil]
----

So in the above statement we have initialized the array with three `nil` values in it. Now lets remove the `nil` values by compacting it as shown below

[source, ruby]
----
>> a.compact
=> [1, 2, 3]
----

Cool! So ruby has removed `nil` values like magic. Now lets see how `a` looks

[source, ruby]
----
>> a
=> [1, nil, 2, nil, 3, nil]
----

To our dismay a still retains the nil  values, what really happened? Well, when you called compact on `a`, a new compacted array was created and it was returned to our REPLfootnote:[https://en.wikipedia.org/wiki/REPL], and out REPL printed it. So what to do to make `a` really change? Simple call compact with a bang or `compact!` on a as shown below ;)

[source, ruby]
----
>> a.compact!
=> [1, 2, 3]
>> a
=> [1, 2, 3]
----

