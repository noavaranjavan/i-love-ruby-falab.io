== Date and Time

Ruby has got ways by which we can extract time and date from our computer clock. All modern day personal computers has got a thing called RTC (real time clock) which is powered by a battery and would maintain the time even if the machine is turned off. Many programming languages let us access this clock and do manipulation with date and time. Lets see how to work with Ruby. Lets do this stuff in our irb rather than writing programs into a file.

The first thing we do is to find whats the time now? For that just type `Time.now`  in your irb, that's it. You will get the current time.

[source, ruby]
----
>> Time.now
=> Thu Feb 25 16:54:45 +0530 2010
----

Time.now is a synonym to `Time.new` which creates a new `Time` object. You can use `Time.now` or `Time.new`, both will give the same result.

[source, ruby]
----
>> Time.new
=> Thu Feb 25 16:54:48 +0530 2010
----

In the following command, we create a new time object / instance and assign it to a variable `t`

[source, ruby]
----
>> t = Time.new
=> Thu Feb 25 16:55:02 +0530 2010
----

Now having assigned, how to view the value of `t`? We will use the inspect method. So by typing `t.inspect`, we can inspect whats in t.

[source, ruby]
----
>> t.inspect
=> "Thu Feb 25 16:55:02 +0530 2010"
----

`t.inspect` converts the time object to a string and displays to us. The same thing can be done by `to_s` (to string) function as shown below

[source, ruby]
----
>> t.to_s
=> "Thu Feb 25 16:55:02 +0530 2010"
----

`t.year` retrieves the year in time object

[source, ruby]
----
>> t.year
=> 2010
----

`t.month` retrieves the month in time object

[source, ruby]
----
>> t.month
=> 2
----

`t.day` retrieves the day (in that month) in time object

[source, ruby]
----
>> t.day
=> 25
----

`t.wday` retrieves the day's number. Here 0 means Sunday, 1 → Monday, 2 → Tuesday and so on till 6 means Saturday

[source, ruby]
----
>> t.wday
=> 4
----

In above code snippet, the day was Thursday.

`t.yday` retrieves the day in that year. For example 1st of February is the 32nd day in the year.

[source, ruby]
----
>> t.yday
=> 56
----

`t.hour` retrieves the hour in the time object. The hour is 24 hour format.

[source, ruby]
----
>> t.hour
=> 16
----

`t.min` retrieves the minutes value in time object.

[source, ruby]
----
>> t.min
=> 55
----

`t.sec` retrieves the seconds in time object.

[source, ruby]
----
>> t.sec
=> 2
----

`t.usec` retrieves microseconds in the time object. This will be useful if you are commissioned to write a stopwatch application for Olympics.

[source, ruby]
----
>> t.usec
=> 357606
----

`t.zone` retrieves the zone. I am in India, we follow Indian Standard Time here, its spelled IST for short.

[source, ruby]
----
>> t.zone
=> "IST"
----

There is a thing called UTC or Universal Time Coordinatefootnote:[https://en.wikipedia.org/wiki/Coordinated_Universal_Time]. Its the time that's at longitude 0 degrees. The `t.utc_offset` displays the number of seconds your time is far away from the time at UTC.

[source, ruby]
----
>> t.utc_offset
=> 19800
----

From the above example, I came to know that a person living at Greenwich will see sunrise after 19800 seconds after I have seen.

DST means daylight saving timefootnote:[https://en.wikipedia.org/wiki/Daylight_saving_time]. I don't know what it is. If your timezone has a daylight saving, this function returns `true`, else `false`.

[source, ruby]
----
>> t.isdst
=> false
----

If your timezone is `UTC`, the `t.utc` returns true or returns false

[source, ruby]
----
>> t.utc?
=> false
----

If you want to get the local time just call the `localtime` function as shown. We want `t` to hold local time value in this case

[source, ruby]
----
>> t.localtime
=> Thu Feb 25 16:55:02 +0530 2010
----

In same way as local time, the `gmtime` function gets the Greenwich Meridian Time.

[source, ruby]
----
>> t.gmtime
=> Thu Feb 25 11:25:02 UTC 2010
----

The `getlocal` function is the alias of `local_time`

[source, ruby]
----
>> t.getlocal
=> Thu Feb 25 16:55:02 +0530 2010
----

The `getutc` function is alias of `gmtime`. Actually `gmtime` is alias of `getutc`

[source, ruby]
----
>> t.getutc
=> Thu Feb 25 11:25:02 UTC 2010
----

The `ctime` function formats the time to somewhat human readable form.

[source, ruby]
----
>> t.ctime
=> "Thu Feb 25 11:25:02 2010"
----

Lets say we want to subtract some seconds from the time value t, we can do it as shown. Below we subtract 86400 seconds (1 day) from our time value

[source, ruby]
----
>> t - 86400
=> Wed Feb 24 11:25:02 UTC 2010
----

=== Days between two days

Lets now write a code snippet that finds the number of days between February 25th 2010 to May 1st 2010, first we declare a variable `a` and assign it with the day February 25th 2010 as shown

[source, ruby]
----
>> a = Time.local 2010, 2, 25
=> Thu Feb 25 00:00:00 +0530 2010
----

Notice we use a function called `local` in `Time` class, we can assign a date to it. As we could see in the output, we get to know that variable `a` now has the value of February 25th . In similar fashion we create a variable `b` and assign it with date 1st of May 2010

[source, ruby]
----
>> b = Time.local 2010, 5, 1
=> Sat May 01 00:00:00 +0530 2010
----

All we do now is subtract a from b

[source, ruby]
----
>> b -a
=> 5616000.0
----

This gives number of seconds between a and b. We divide the result by 86400 (that's how many seconds that are in a day)

[source, ruby]
----
>> days = _ / 86400
=> 65.0
----

We get a result as 65.

=== How many days have you lived?

Lets now see a program that takes in your birthday and prints out how many days have you lived. Type in the program in text editor and execute it

[source, ruby]
----
include::code/how_many_days.rb[]
----

Here is the result

----
Enter birthday (YYYY-MM-DD):2000-5-23
You have lived for 3566 days
----

Well this may vary on when you execute this program. Lets now analyze it. In the first line

[source, ruby]
----
print "Enter birthday (YYYY-MM-DD):"
----

We ask the user to enter his or her birthday, once done we perform a trick here. We asked the user to enter it in YYYY-MM-DD format, in the statement

[source, ruby]
----
bday = gets.chop
----

We get the date and store it in a variable called `bday`. The `gets.chop` gets the birth day and chops off the enter sign we enter with it. So `bday` now holds the string value of birthday you entered. In the next statement

[source, ruby]
----
year, month, day = bday.split('-')
----

we have a multiple assignment in which I have three variables `year`, `month` and `day`. I am splitting the birthday string and assigning it. What really happens is this, if we enter a date like 1994-6-24 it gets split by `–` and becomes an array which is shown in code snippet below executed in irb

[source, ruby]
----
>> "1994-6-24".split '-'
=> ["1994", "6", "24"]
----
Lets assign this array to variables `a`, `b`, `c` simultaneously as shown

[source, ruby]
----
>> a, b, c = _
=> ["1994", "6", "24"]
----

If you remember `_` (underscore) means the last obtained result in irb. So having assigned it we now check values of `a`, `b` and `c` which we get as shown....

[source, ruby]
----
>> a
=> "1994"
>> b
=> "6"
>> c
=> "24"
----

In similar fashion in

[source, ruby]
----
year, month, day = bday.split('-')
----

The `year` variable gets the year part, the `month` gets the month and `day` gets the day. OK having obtained the parameters of a particular day we can proceed. Examine the following statement

[source, ruby]
----
seconds =  Time.now - Time.local(year, month, day)
----

See the right hand side of the equal to sign, first we have `Time.now` which gets the current time, from it we are subtracting a time object that's been created using `Time.local`. `Time.local` can be used to create a time object that's fixed at any instance, the instance can be past, present or future. We pass the `year`, `month` and `day` to it to create a `Time` object. What happens when we subtract these both, we get the difference in seconds which gets stored in variable called `seconds` at the left hand side of the equation.

All we need to do now is to  convert the second to days which is done by the following statement

[source, ruby]
----
days = (seconds / 86400).round
----

Here we divide seconds by 86400 which converts them to days. We might be getting some value like 378.567 days, to get rid of the .567 we round it off using the round function, so `(seconds / 86400).round` returns a neat rounded value which can be read by humans quiet easily. We store the value in a variable called `days`. Finally we print the fact that we have lived for so many long days using the following statement

[source, ruby]
----
puts "You have lived for #{days} days"
----

Well, that's it.

I would like to tell one thing I found out with `Time.local` function, its not that we must pass only numbers to it as shown

[source, ruby]
----
>> Time.local "2010", "5", "1"
=> Sat May 01 00:00:00 +0530 2010
----

We can pass a bit human friendly values as shown below. Instead of putting 5 for month, we use May.

[source, ruby]
----
>>Time.local "2010", "may", "1"
=> Sat May 01 00:00:00 +0530 2010
----

Some times Ruby language looks as easy as talking English.

